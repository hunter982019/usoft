import { RedisIoAdapter } from "./redis-io.adapter";
import { Module } from "@nestjs/common";

@Module({
  providers: [RedisIoAdapter],
  exports: [RedisIoAdapter]
})
export class SocketIoModule{}