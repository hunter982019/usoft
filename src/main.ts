import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import  {SwaggerModule, DocumentBuilder} from '@nestjs/swagger'
import { RedisIoAdapter } from "./socket-io/redis-io.adapter";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // const redisIoAdapter = new RedisIoAdapter(app)
  // await redisIoAdapter.connectToRedis();
  // app.useWebSocketAdapter(redisIoAdapter);

  const config = new DocumentBuilder()
    .setTitle('Restfull api example')
    .setDescription('UniconSoft')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/docs', app, document);
  await app.listen(3000);
}
bootstrap();
