import {  Injectable, UseGuards } from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { Knex } from "knex";
import { InjectModel } from "nest-knexjs";
import { User } from "./entities/user.entity";
import { RolesGuard } from "../guard/roles.guard";


@Injectable()
@UseGuards(RolesGuard)
export class UsersService {

  constructor(@InjectModel() private readonly knex: Knex) {}


 async create(createUserDto: CreateUserDto) {
     return this.knex.table('users')
       .insert(createUserDto)
       .returning("*");
 }


 async findAll(): Promise<User[]> {
   return this.knex.select("*")
     .from('users');
  }

 async findOne(id: number):Promise<User> {
    return this.knex.select("*")
      .where({"id":id})
      .from("users").first();
  }

 async update(id: number, updateUserDto: UpdateUserDto):Promise<number> {
    return this.knex.where({"id":id})
      .from("users")
      .update(updateUserDto)
  }

 async remove(id: number):Promise<number> {
    return this.knex.where({"id":id})
      .from("users")
      .delete();
  }

  async findMyProjects(id: number){
    return this.knex.select("*")
      .from('Project')
      .where('Project.created_by', '=', id)
      .innerJoin('Task','Project.created_by', '=', 'Task.created_by')
  }

  async  complexQuery(){
    //This query takes all posts gerenerates new object to write owner info
    // const data = await  this.knex.from('Post')
    //   .where('userId', 1)
    //   .select('Post.*', 'users.name', 'users.role')
    //   .join('users', 'Post.userId', 'users.id');
    //
    // const transformedResult = data.map((raw) => {
    //   return {
    //     id:raw.id,
    //     title: raw.title,
    //     body: raw.body,
    //     createdAt: raw.created_at,
    //     owner: {
    //       id: raw.userId,
    //       name: raw.name,
    //       role: raw.role
    //     }
    //   }
    // })

    //This query takes all users where posts not null and more zero
    // const result = await this.knex('users')
    //   .select('users.id', 'users.name', 'subquery.userId', "subquery.posts_count")
    //   .leftJoin(
    //     this.knex.raw('(SELECT "userId", COUNT(*)::int as posts_count FROM "Post" GROUP BY "userId") as subquery'),
    //     'users.id',
    //     'subquery.userId'
    //   )
    //   .whereNotNull('subquery.posts_count')
    //   .andWhere('subquery.posts_count', '>', 0);

    const result = await  this.knex("Post")
      .where("userId", 1)
      .select("id", "title");
    return result;
  }


}
