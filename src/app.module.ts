import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { DatabaseModule } from "./db/knex.module";
import { GuardModule } from "./guard/guard.module";
import { OrganizationsModule } from './organizations/organizations.module';
import { OrganizationUserModule } from './organization-user/organization-user.module';
import { ProjectModule } from './project/project.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CronModule } from "./crons/cron.module";
import { SharedModule } from "./shared/shared.module";
import { BullModule } from "@nestjs/bull";

@Module({
  imports: [
    ScheduleModule.forRoot(),
    UsersModule,
    DatabaseModule,
    GuardModule,
    OrganizationsModule,
    OrganizationUserModule,
    ProjectModule,
    SharedModule,
    CronModule,
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
  ],
  controllers: [],
  providers: [AppService],
})
export class AppModule {}
