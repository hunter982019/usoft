import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable('albums', function (table: any) {
      table.increments('id').notNullable().unique();
      table.integer('userId').notNullable();
      table.string('title');
      table.timestamp('createdAt').defaultTo(knex.fn.now());
    })
}


export async function down(knex: Knex): Promise<void> {
}

