import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable('Post', function (table: any) {
      table.increments('id').notNullable().unique();
      table.integer('userId').notNullable();
      table.string('title');
      table.string('body');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.integer('done_at');
      table.string('status');
    })
}


export async function down(knex: Knex): Promise<void> {
}

