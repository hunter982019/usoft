import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable('photos', function (table: any) {
      table.increments('id').notNullable().unique();
      table.integer('albumId').notNullable();
      table.string('title');
      table.string('url');
      table.string('thumbnailUrl');
      table.timestamp('createdAt').defaultTo(knex.fn.now());
    })
}


export async function down(knex: Knex): Promise<void> {
}

