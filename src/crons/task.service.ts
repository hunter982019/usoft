import { Inject, Injectable, Logger } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { CustomHttpService } from "../shared/http.service";
import { InjectModel } from "nest-knexjs";
import { Knex } from "knex";
import { TaskRepository } from "./task.repository";

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  @Inject() httpService: CustomHttpService;

  constructor(@InjectModel() private readonly knex: Knex) {}

  @Cron('11 * * * * *')
  async handleCron() {
      const dataPost = await this.getPosts();
     await this.knex.transaction(async function(trx)  {
       let lastElement = await trx('Post')
         .orderBy('id', 'desc')
         .first();
        if(lastElement === undefined){
          lastElement = {id:0};
        }
       let notWritten = dataPost.filter((val) => {
          if(val?.id > lastElement.id){
            return true;
          }
       });
        if(notWritten.length > 0)
          await trx('Post').insert(notWritten);
        else
          console.log('Everythiung clear')
     });

    this.logger.log('Cron task is called');
  }

  async getPosts(){
    console.log("Running")
    return   await this.httpService.get('https://jsonplaceholder.typicode.com/posts');
  }
  // @Cron(CronExpression.EVERY_5_SECONDS)
  async updatePhotosByDelete(){
    try {
      const photos = await this.getPhotos();
      await this.knex.transaction(async (trx) => {
            await trx('photos').del();
            await trx('photos').insert(photos)
      })
    }catch (e) {
      console.log(`smThing wrong at updatePhotosByDelete ${e}`)
    }
  }
  async  getPhotos(){
    return await this.httpService.get('https://jsonplaceholder.typicode.com/photos')
  }
  // @Cron(CronExpression.EVERY_5_SECONDS)
  async writeAlbums(){
    try {
      const albums = await  this.getAlbums();
      await this.knex.transaction(async (trx) => {
        await trx('albums').del();
        await trx('albums').insert(albums)
      })
    }catch (e) {
      console.log(`smThing wrong at writeAlbums ${e}`)
    }

  }
  async getAlbums(){
    return await  this.httpService.get('https://jsonplaceholder.typicode.com/albums')
  }
  async filterData(){
    return this.knex
      .with('with_alias',['userId'], (qb) => {
        qb.select('*')
          .from('users')
          .where('id', 1)
      })
      .select('*')
      .from('with_alias')
  }
}