import { Controller, Get, Inject } from "@nestjs/common";
import { TasksService } from "./task.service";

@Controller('task')
export class TaskController{
  @Inject() taskService:TasksService;

  @Get()
 async getFileredData(){
    try {
      return this.taskService.filterData();
    }catch (e) {
      throw new Error(`myError: ${e} `)
    }
  }
}