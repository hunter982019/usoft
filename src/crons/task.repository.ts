import { Injectable } from "@nestjs/common";
import { BaseRepositoryService, BaseTransaction } from "../shared/baseRepository.service";


@Injectable()
export  class TaskRepository extends BaseRepositoryService{
  transacting(trx: BaseTransaction): TaskRepository {
    const collectionRepositoryTransaction:TaskRepository = new TaskRepository(
      trx.transaction,
    );
    return collectionRepositoryTransaction;
  }

  async findByIds(ids: number) {

    return this.knex.select("*")
      .where({"id":ids})
      .from("users").first();
  }

}