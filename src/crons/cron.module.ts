import { Module } from "@nestjs/common";
import { TasksService } from "./task.service";
import { SharedModule } from "../shared/shared.module";
import { TaskController } from "./task.controller";



@Module({
  controllers: [TaskController],
  imports:[SharedModule],
  exports: [TasksService],
  providers: [TasksService]
})
export class CronModule{}