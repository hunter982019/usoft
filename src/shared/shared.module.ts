import { Module } from "@nestjs/common";
import { HttpModule } from "@nestjs/axios";
import { CustomHttpService } from "./http.service";



@Module({
  imports: [HttpModule],
  exports: [CustomHttpService],
  providers: [CustomHttpService]
})
export class SharedModule{}